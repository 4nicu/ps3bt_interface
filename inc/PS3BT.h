#pragma once
/*********************************************/
/*          Include Header                   */
/*********************************************/
#include <string.h>
#include <uart.h>

#define MAXSTRLEN	30
#define PS3BTMAXLEN	30

#define	PB1  PORTEbits.RE8
#define	PB2	 PORTEbits.RE9
#define LED1 LATAbits.LATA6
#define LED2 LATAbits.LATA7
#define LED3 LATEbits.LATE0

#define SELECT	 0x01
#define	L3		 0x02
#define R3		 0x04
#define START	 0x08
#define UP		 0x10
#define RIGHT	 0x20
#define DOWN	 0x40
#define LEFT	 0x80
#define L2		 0x0100
#define R2		 0x0200
#define L1  	 0x0400
#define R1 		 0x0800
#define TRIANGLE 0x1000
#define CIRCLE 	 0x2000
#define CROSS 	 0x4000
#define SQUARE 	 0x8000    
#define PS		 0x010000

#define ps3_low_Lx		115.0						
#define ps3_high_Lx		140.0

#define ps3_low_Ly		115.0
#define ps3_high_Ly		140.0

#define ps3_low_Rx		115.0						
#define ps3_high_Rx		140.0

#define ps3_low_Ry		115.0
#define ps3_high_Ry		140.0	

#define joyL_up			(float)ps3bt->leftjoy_y < ps3_low_Ly
#define joyL_down		(float)ps3bt->leftjoy_y > ps3_high_Ly
#define joyL_left		(float)ps3bt->leftjoy_x < ps3_low_Lx
#define joyL_right		(float)ps3bt->leftjoy_x > ps3_high_Lx

#define joyR_up			(float)ps3bt->rightjoy_y < ps3_low_Ry
#define joyR_down		(float)ps3bt->rightjoy_y > ps3_high_Ry
#define joyR_left		(float)ps3bt->rightjoy_x < ps3_low_Rx
#define joyR_right		(float)ps3bt->rightjoy_x > ps3_high_Rx

/*********************************************/
/*         ENUM		              			 */
/*********************************************/
enum psLED {psLED1 = 0x01, psLED2 = 0x02, psLED3 = 0x04, psLED4 = 0x08};
enum psRUMBLE {RumbleHigh = 0x40, RumbleLow = 0x20};

/*********************************************/
/*          Struct              			 */
/*********************************************/
typedef struct{
	char data;
	
	unsigned char x;
	
	unsigned int leftjoy_x;
	unsigned int leftjoy_y; 
	unsigned int rightjoy_x;
	unsigned int rightjoy_y;
	
	unsigned int an_L2;
	unsigned int an_R2;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
	
	float joyR_y;
	float joyR_x;
	float joyL_y; 
	float joyL_x;
	
	unsigned char command;
	unsigned int button;
	char sendBuffer[5];
	char ReceiveBuffer[PS3BTMAXLEN];
}PS3BT_t;

/*********************************************/
/*          Function Prototype         		 */
/*********************************************/
void PS3BTGetXY(PS3BT_t *ps3bt);
void PS3BTInit(PS3BT_t *ps3bt);
void PS3BTReceive(PS3BT_t *ps3bt);
void PS3BTEnquire(PS3BT_t *ps3bt);
void PSLEDOn(enum psLED, PS3BT_t *ps3bt);
void PSLEDOff(enum psLED, PS3BT_t *ps3bt);
void PSRumble(enum psRUMBLE, PS3BT_t *ps3bt);
void PS3BTHandler(PS3BT_t *ps3bt);