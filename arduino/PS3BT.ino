/*
Example sketch for the PS3 Bluetooth library - developed by Kristian Lauszus
For more information visit my blog: http://blog.tkjelectronics.dk/ or
send me an e-mail:  kristianl@tkjelectronics.com
*/
/****************************************************************************
* Title   : PS3 Bluetooth
* Author  : Nicole Tham
* Version : 1.5
* Date    : 15 Nov, 2014
*****************************************************************************
* Description:
*
* Version History:
* 1.5 - Reduced delay of LED and rumble commands
* Bugs:
* - Excessive rumble calling will cause Arduino to hang
******************************************************************************/
#include <TimerOne.h>
#include <PS3BT.h>
USB Usb;
BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so
/* You can create the instance of the class in two ways */
PS3BT PS3(&Btd); // This will just create the instance
//PS3BT PS3(&Btd,0x00,0x15,0x83,0x3D,0x0A,0x57); 
// This will also store the bluetooth address - this can be obtained from the dongle when running the sketch

#define Rpin 3
#define DataEnquiry   commandBuf&0x80
#define setRumbleHigh commandBuf&0x40
#define setRumbleLow  commandBuf&0x20
#define setLED        commandBuf&0x10

boolean hidcmd;
boolean start_count = false;
int jiffies = 0;
char *buf, buf1[30], buf2[30];
unsigned char commandBuf;

typedef struct {
  unsigned int button;
  unsigned char joyLx, joyLy, joyRx, joyRy;
  unsigned char AL2, AR2;
} PS3BT_t;
PS3BT_t ps3bt;

void getAnalog(PS3BT_t *ps3bt);
void twowayComm(PS3BT_t ps3bt);
void getData(PS3BT_t *ps3bt);

void setup() {
  Serial.begin(115200);
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); //halt
  }
  Serial.print(F("\r\nPS3 Bluetooth Library Started "));
  pinMode(Rpin, OUTPUT);
  digitalWrite(Rpin,  HIGH);
  Timer1.initialize(1000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
  Timer1.attachInterrupt( timerIsr ); // attach the service routine here
  buf = buf1;
}

void loop() {
  Usb.Task();
  if (PS3.PS3Connected || PS3.PS3NavigationConnected) {
    twowayComm(ps3bt);

    if (PS3.getButtonPress(PS)) {
      digitalWrite(Rpin,  LOW);
    } 
    else {
      digitalWrite(Rpin,  HIGH);
      if (PS3.getButtonPress(TRIANGLE))
        ps3bt.button |= TRIANGLE;
      else
        ps3bt.button &= ~TRIANGLE;

      if (PS3.getButtonPress(CIRCLE))
        ps3bt.button |= CIRCLE;
      else
        ps3bt.button &= ~CIRCLE;

      if (PS3.getButtonPress(CROSS))
        ps3bt.button |= CROSS;
      else
        ps3bt.button &= ~CROSS;

      if (PS3.getButtonPress(SQUARE))
        ps3bt.button |= SQUARE;
      else
        ps3bt.button &= ~SQUARE;

      if (PS3.getButtonPress(UP))
        ps3bt.button |= UP;
      else
        ps3bt.button &= ~UP;

      if (PS3.getButtonPress(RIGHT))
        ps3bt.button |= RIGHT;
      else
        ps3bt.button &= ~RIGHT;

      if (PS3.getButtonPress(DOWN))
        ps3bt.button |= DOWN;
      else
        ps3bt.button &= ~DOWN;

      if (PS3.getButtonPress(LEFT))
        ps3bt.button |= LEFT;
      else
        ps3bt.button &= ~LEFT;

      if (PS3.getButtonPress(L1))
        ps3bt.button |= L1;
      else
        ps3bt.button &= ~L1;

      if (PS3.getButtonPress(L3))
        ps3bt.button |= L3;
      else
        ps3bt.button &= ~L3;

      if (PS3.getButtonPress(R1))
        ps3bt.button |= R1;
      else
        ps3bt.button &= ~R1;

      if (PS3.getButtonPress(R3))
        ps3bt.button |= R3;
      else
        ps3bt.button &= ~R3;

      if (PS3.getButtonPress(SELECT))
        ps3bt.button |= SELECT;
      else
        ps3bt.button &= ~SELECT;

      if (PS3.getButtonPress(START))
        ps3bt.button |= START;
      else
        ps3bt.button &= ~ START;

      if (PS3.getButtonPress(L2))
        ps3bt.button |= L2;
      else
        ps3bt.button &= ~ L2;

      if (PS3.getButtonPress(R2))
        ps3bt.button |= R2;
      else
        ps3bt.button &= ~ R2;
        
      getAnalog(&ps3bt);
    }
  }
}

void getAnalog(PS3BT_t *ps3bt)
{
  ps3bt->joyLx  = PS3.getAnalogHat(LeftHatX);
  ps3bt->joyLy  = PS3.getAnalogHat(LeftHatY);
  ps3bt->joyRx = PS3.getAnalogHat(RightHatX);
  ps3bt->joyRy = PS3.getAnalogHat(RightHatY);
  ps3bt->AL2 = PS3.getAnalogButton(L2_ANALOG);
  ps3bt->AR2 = PS3.getAnalogButton(R2_ANALOG);
}

void twowayComm(PS3BT_t ps3bt)
{
  sprintf(buf, "%u %d %d %d %d %d %d\r\n", ps3bt.button, ps3bt.joyLx, ps3bt.joyLy, ps3bt.joyRx, ps3bt.joyRy, ps3bt.AL2, ps3bt.AR2);

  if(Serial.available()>0){
    commandBuf = Serial.read();
    if (DataEnquiry) {
     // Serial.println("Enquire");
     if (buf == buf1) {
        buf = buf2;
        Serial.print(buf2);
      } else {
        buf = buf1;
        Serial.print(buf1);
      }
    }

  if (setLED) {
      if(commandBuf&LED1){ 
        PS3.mysetLedOn(LED1);
        hidcmd = true;
      }else if((~commandBuf)&LED1){
        PS3.mysetLedOff(LED1);
        hidcmd = true;
      }
   
      if(commandBuf&LED2){
        PS3.mysetLedOn(LED2);
        hidcmd = true;
      }else if(~commandBuf&LED2){
        PS3.mysetLedOff(LED2);
        hidcmd = true;
      }
      
    if(commandBuf&LED3){
       PS3.mysetLedOn(LED3);
       hidcmd = true;
    }else if(~commandBuf&LED3){ 
      PS3.mysetLedOff(LED3);
        hidcmd = true;
    }
    
    if(commandBuf&LED4){
       PS3.mysetLedOn(LED4);
       hidcmd = true;
    }else if(~commandBuf&LED4){
      PS3.mysetLedOff(LED4);
        hidcmd = true;
    }
  }
  
    if (setRumbleHigh) {      
      PS3.setRumbleOn(RumbleHigh);
      start_count = true;
    }
  
    if (setRumbleLow) {
      PS3.setRumbleOn (RumbleLow);
      start_count = true;
    }
    
    if(hidcmd == true){
      hidcmd = false;
      PS3.sendHID_Command();
    }
  }
}

void timerIsr()
{
  if (PS3.PS3Connected || PS3.PS3NavigationConnected) {  
    if(start_count == true && jiffies < 300){
      jiffies++;
    }
    if(jiffies >= 300){
      start_count = false;
      jiffies = 0;
      PS3.setRumbleOff(); 
    }
  }
}


