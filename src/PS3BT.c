/*******************************************************************************
 * Title   : PS3 Bluetooth Interface
 * Author  : Nicole Tham
 * Version : 1.5
 * Date    : 13/12/2014
 *******************************************************************************
 * Description:
 * 
 * Version History:
 * 1.5
 * - LED & Rumble functions (user-set flag)
 * - PS3BT Handler function
 * 1.6
 * - Reduced delay of LED & rumble commands
 * - LED command set to send only once per bit change
 * Bugs:
 *  - Excessive rumble calling will cause Arduino to hang
 ******************************************************************************/
#include <PS3BT.h>
char PSdata;

void PS3BTReceive(PS3BT_t *ps3bt)
{
	PSdata = ReadUART2();
	ps3bt->ReceiveBuffer[ps3bt->x++] = PSdata;
}

void PS3BTHandler(PS3BT_t *ps3bt)
{
	char i;
	if(PSdata == '\n'){
		for (i = ps3bt->x; i < PS3BTMAXLEN; i++){
			ps3bt->ReceiveBuffer[i] = '\0';
		}	    
		ps3bt->x = 0;
		sscanf(ps3bt->ReceiveBuffer,"%u %d %d %d %d %d %d", &ps3bt->button, &ps3bt->leftjoy_x, &ps3bt->leftjoy_y, &ps3bt->rightjoy_x, &ps3bt->rightjoy_y, &ps3bt->an_L2, &ps3bt->an_R2); 
//		sprintf(buf,"%u %d %d %d %d\r\n", ps3bt->button, ps3bt->leftjoy_x, ps3bt->leftjoy_y, ps3bt->rightjoy_x, ps3bt->rightjoy_y);
		PS3BTGetXY(ps3bt); 
//		sprintf(buf,"%.2f %.2f %.2f %.2f\r\n", ps3bt->joyR_y, ps3bt->joyR_x, ps3bt->joyL_y, ps3bt->joyL_x);
//		putsUART1(buf);
	}else{
	}
}

void PS3BTEnquire(PS3BT_t *ps3bt)
{
	if(ps3bt->command & 0x10){
		ps3bt->command &= ~0x80;
	}else{
		ps3bt->command |= 0x80;
	}	
		
	sprintf(ps3bt->sendBuffer, "%c", ps3bt->command);
	putsUART2(ps3bt->sendBuffer);
	
	if(ps3bt->command & RumbleHigh){
		ps3bt->command &= ~RumbleHigh;
	}
	if(ps3bt->command & RumbleLow){
		ps3bt->command &= ~RumbleLow;
	}
	if(ps3bt->command & 0x10){
		ps3bt->command &= ~0x10;
	}    
}

void PSLEDOn(enum psLED x, PS3BT_t *ps3bt)
{
	if(!(ps3bt->command & x)){
		ps3bt->command |= x;
		ps3bt->command |= 0x10;
	}
}

void PSLEDOff(enum psLED a, PS3BT_t *ps3bt)
{
	if(ps3bt->command & a){
		ps3bt->command &= ~a;
		ps3bt->command |= 0x10;
	}
}

void PSRumble(enum psRUMBLE b, PS3BT_t *ps3bt)
{
	ps3bt->command |= b;
}

void PS3BTGetXY(PS3BT_t *ps3bt)
{
	if(joyR_up){
 		ps3bt->joyR_y  = ((ps3_low_Ry - ((float)ps3bt->rightjoy_y))/100.0)/1.00;
		if(ps3bt->joyR_y < 0.0){
			ps3bt->joyR_y = 0.0;
		}else if(ps3bt->joyR_y > 1.0){
			ps3bt->joyR_y = 1.0;
		}
	}else if(joyR_down){
		ps3bt->joyR_y = ((ps3_high_Ry - ((float)ps3bt->rightjoy_y))/100.0)/1.00;
		if(ps3bt->joyR_y > 0.0){
			ps3bt->joyR_y = 0.0;
 		}else if(ps3bt->joyR_y < -1.0){
			ps3bt->joyR_y = -1.0;
		}
	}else{
		ps3bt->joyR_y = 0.0;
	}
	
	if(joyR_left){
		ps3bt->joyR_x = ((ps3_low_Rx - ((float)ps3bt->rightjoy_x))/100.0)/1.00;
		if(ps3bt->joyR_x < 0.0){
			ps3bt->joyR_x = 0.0;
		}else if(ps3bt->joyR_x > 1.0){
			ps3bt->joyR_x = 1.0;
		}
	}else if(joyR_right){
		ps3bt->joyR_x = ((ps3_high_Rx - ((float)ps3bt->rightjoy_x))/100.0)/1.00;
		if(ps3bt->joyR_x > 0.0){
			ps3bt->joyR_x = 0.0;
		}else if(ps3bt->joyR_x < -1.0){
			ps3bt->joyR_x = -1.0;
		}
	}else{
		ps3bt->joyR_x = 0.0;
	}
	
	if(joyL_up){
		ps3bt->joyL_y = ((ps3_low_Ly - ((float)ps3bt->leftjoy_y))/100.0)/1.00;
		if(ps3bt->joyL_y < 0.0){
			ps3bt->joyL_y = 0.0;
		}else if(ps3bt->joyL_y > 1.0){
			ps3bt->joyL_y = 1.0;
		}
	}else if(joyL_down){
		ps3bt->joyL_y = ((ps3_high_Ly - ((float)ps3bt->leftjoy_y))/100.0)/1.00;
		if(ps3bt->joyL_y > 0.0){
			ps3bt->joyL_y = 0.0;
		}else if(ps3bt->joyL_y < -1.0){
			ps3bt->joyL_y = -1.0;
		}
	}else{
		ps3bt->joyL_y = 0.0;
	}
	
	if(joyL_left){
		ps3bt->joyL_x = ((ps3_low_Lx - ((float)ps3bt->leftjoy_x))/100.0)/1.00;
		if(ps3bt->joyL_x < 0.0){
			ps3bt->joyL_x = 0.0;
		}else if(ps3bt->joyL_x > 1.0){
			ps3bt->joyL_x = 1.0;
		}
	}else if(joyL_right){
		ps3bt->joyL_x = ((ps3_high_Lx - ((float)ps3bt->leftjoy_x))/100.0)/1.00;
		if(ps3bt->joyL_x > 0.0){
			ps3bt->joyL_x = 0.0;
		}else if(ps3bt->joyL_x < -1.0){
			ps3bt->joyL_x = -1.0;
		}
	}else{
		ps3bt->joyL_x = 0.0;
	}
}

void PS3BTInit(PS3BT_t *ps3bt)
{
	ps3bt = (PS3BT_t *) malloc(sizeof(PS3BT_t));
}	