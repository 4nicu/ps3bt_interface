# dsPIC33f <-> Arduino UART interface for PS3 Bluetooth controller

An old code I made for the ABU Asia Pacific Robot Competition Pune, India. It’s a library that handles data obtained from a wireless PS3 Bluetooth controller which is connected to the Arduino and our dsPIC33f based motherboard where most of the overall robot main program is executed. 
This was an attempt to write a C library in OOP style for ease of use.

## Example usage:
### Include library
```
#include <PS3BT.h>
extern PS3BT_t PS3bt
```
### In timer interrupt:
```C
PS3Handler(&PS3bt); // Get buttons value
PS3HandlerGetXY(&PS3bt); // Get Analog joystick value
```
### In UART RX interrupt:
```C
PS3BTReceive(&PS3bt); // Read data from UART buffer
```
To get button press information for example, PS3bt.button stores all 16 buttons press state in 16 bits.
